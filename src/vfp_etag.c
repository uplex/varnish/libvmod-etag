/*-
 * Copyright 2017,2019 UPLEX - Nils Goroll Systemoptimierung
 * All rights reserved.
 *
 * Author: Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

#include "config.h"

#include <stdlib.h>
#include <string.h>

#include "cache/cache.h"
#include "cache/cache_filter.h"
#include "vsha256.h"
#include "miniobj.h"

const struct vfp VFP_etag;

struct etag {
	unsigned		magic;
#define BODYHASH_MAGIC		0xb0d16a56

	struct VSHA256Context	sha256ctx;
};

const char const placeholder[] =
    "\"vetag"					\
    "VMOD-ETAG MAGIC ETAGPLACEHOLDER "	\
    "VMOD-ETAG MAGIC ETAG PLACEHOLDER\"";
const size_t placeholder_l = sizeof(placeholder) - 1;

static enum vfp_status
vfp_etag_init(VRT_CTX, struct vfp_ctx *vc, struct vfp_entry *vfe)
{
	struct etag *bh = NULL;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(vc, VFP_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(vfe, VFP_ENTRY_MAGIC);

	assert(vfe->vfp == &VFP_etag);
	assert(placeholder_l == VSHA256_LEN * 2 + 2 /* " */ + 5 /* vetag */);
	AZ(vfe->priv1);

	AN(vc->resp);
	bh = WS_Alloc(vc->resp->ws, sizeof(*bh));
	if (bh == NULL)
		return (VFP_ERROR);

	INIT_OBJ(bh, BODYHASH_MAGIC);
	VSHA256_Init(&bh->sha256ctx);

	vfe->priv1 = bh;
	http_ForceHeader(vc->resp, H_ETag, placeholder);
	return (VFP_OK);
}

static enum vfp_status
vfp_etag_pull(struct vfp_ctx *vc, struct vfp_entry *vfe, void *p,
    ssize_t *lp)
{
	struct etag *bh;
	enum vfp_status vp;

	CHECK_OBJ_NOTNULL(vc, VFP_CTX_MAGIC);
	AN(lp);

	vp = VFP_Suck(vc, p, lp);
	if (*lp <= 0)
		return (vp);

	CHECK_OBJ_NOTNULL(vfe, VFP_ENTRY_MAGIC);
	CAST_OBJ_NOTNULL(bh, vfe->priv1, BODYHASH_MAGIC);
	VSHA256_Update(&bh->sha256ctx, p, *lp);
	return (vp);
}

const char const hexe[16] = {
	"0123456789abcdef",
};

static void
vfp_etag_fini(struct vfp_ctx *vc, struct vfp_entry *vfe)
{
	struct etag *bh;
	unsigned char	sha[VSHA256_LEN];
	char		*etag, *lim;
	int		i;
	ssize_t		l;

	CHECK_OBJ_NOTNULL(vc, VFP_CTX_MAGIC);
	CHECK_OBJ_NOTNULL(vfe, VFP_ENTRY_MAGIC);

	if (vfe->priv1 == NULL)
		return;

	CAST_OBJ_NOTNULL(bh, vfe->priv1, BODYHASH_MAGIC);
	vfe->priv1 = NULL;

	// HACKY
	etag = TRUST_ME(ObjGetAttr(vc->wrk, vc->oc, OA_HEADERS, &l));
	if (etag == NULL) {
		VSLb(vc->wrk->vsl, SLT_Error, "etag: no object");
		return;
	}
	lim = etag + l;
	do {
		etag = memchr(etag, '"', l);
		if (etag == NULL)
			break;
		l = lim - etag;
		if (l < placeholder_l) {
			etag = NULL;
			break;
		}
		if (strncmp(etag, placeholder, placeholder_l) == 0)
			break;
		etag++;
	} while(1);

	if (etag == NULL) {
		VSLb(vc->wrk->vsl, SLT_Error, "etag: no placeholder");
		return;
	}

	VSHA256_Final(sha, &bh->sha256ctx);

	assert(etag[0] == '"' &&
	    etag[1] == 'v' &&
	    etag[2] == 'e' &&
	    etag[3] == 't' &&
	    etag[4] == 'a' &&
	    etag[5] == 'g');

	etag += 6;
	for (i = 0; i < VSHA256_LEN; i++) {
		*etag++ = hexe[(sha[i] & 0xf0) >> 4];
		*etag++ = hexe[sha[i] & 0x0f];
	}
	*etag++ = '"';
	assert(*etag == '\0');
}

const struct vfp VFP_etag = {
	.name = "etag",
	.init = vfp_etag_init,
	.pull = vfp_etag_pull,
	.fini = vfp_etag_fini
};
