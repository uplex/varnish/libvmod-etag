#include "config.h"

#include <cache/cache.h>
#include <cache/cache_filter.h>

#include "vcc_etag_if.h"
#include "vfp_etag.h"

int
vmod_event_function(VRT_CTX, struct vmod_priv *priv, enum vcl_event_e e)
{
	const char *err;

	switch (e) {
	case VCL_EVENT_LOAD:
		err = VRT_AddFilter(ctx, &VFP_etag, NULL);
		if (err != NULL) {
			VRT_fail(ctx, "Adding etag filter failed: %s",
			    err);
			return (1);
		}
		break;
	case VCL_EVENT_DISCARD:
		VRT_RemoveFilter(ctx, &VFP_etag, NULL);
		break;
	default:
		break;
	}

	return (0);
}
